import { Component, OnInit, Input, ViewChild, ElementRef, Renderer2 } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { ApiService } from '../api.service';
import { ModalController } from '@ionic/angular';
declare var mobilecheck;

@Component({
  selector: 'app-gallery',
  templateUrl: './gallery.page.html',
  styleUrls: ['./gallery.page.scss'],
})
export class GalleryPage implements OnInit {

    isMobile = mobilecheck();
  @Input() contents:any = [];
  data:any;
  @ViewChild('content') c: ElementRef;
  //@ViewChild()
  youtube:boolean = true;
  dataid;
  blk;
  tmpdiv;
  constructor(private api:ApiService, private mod:ModalController, private dom:DomSanitizer, private render:Renderer2) {

  }

  ngOnInit() {
    //console.log(this.contents);
    if(this.contents.link.indexOf('youtube') < 0){
      this.youtube = false;      
      this.tmpdiv = document.createElement('div');
      this.tmpdiv.innerHTML = this.contents.link;

    }else{
      this.data = this.dom.bypassSecurityTrustResourceUrl(this.contents.link.replace('watch?v=','embed/'));
    }
  }

  ngAfterViewInit(){
    //console.log(this.c,this.c.nativeElement);
    if(!this.youtube){
      //console.log(blk);
      let scr = document.createElement('script');
      scr.async = true;
      scr.charset = 'utf-8';
      scr.src = 'https://s.imgur.com/min/embed.js';
      this.render.appendChild(this.c.nativeElement,this.tmpdiv);      
      this.render.appendChild(this.c.nativeElement,scr);      
    }
    
  }

  dismissModal(){
    this.mod.dismiss();
  }

}
