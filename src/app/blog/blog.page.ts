import { Component, OnInit, Input } from '@angular/core';
import { ApiService } from '../api.service';
import { ModalController } from '@ionic/angular';
import { Router, ActivatedRoute } from '@angular/router';
declare var mobilecheck;

@Component({
  selector: 'app-blog',
  templateUrl: './blog.page.html',
  styleUrls: ['./blog.page.scss'],
})
export class BlogPage implements OnInit {
  id: string|number;
    isMobile = mobilecheck();
  // @Input('id') id = 1;
  contents:any = [];
  constructor(private api:ApiService, private mod:ModalController, private route: ActivatedRoute) { }

  ngOnInit() {
    this.route.params.subscribe(params => {
      this.id = params['id'];
      
      this.api.getBlog(this.id).subscribe((t)=>{
        this.contents = t;
        this.contents.url = `url('${this.contents.img}')`;
      })
    })
  }

  dismissModal(){
    this.mod.dismiss();
  }

}
