import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { LightboxPageRoutingModule } from './lightbox-routing.module';

import { LightboxPage } from './lightbox.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    LightboxPageRoutingModule
  ],
  declarations: [LightboxPage]
})
export class LightboxPageModule {}
