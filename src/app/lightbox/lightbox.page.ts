import { Component, OnInit, Input } from '@angular/core';
import { ModalController } from '@ionic/angular';

@Component({
  selector: 'app-lightbox',
  templateUrl: './lightbox.page.html',
  styleUrls: ['./lightbox.page.scss'],
})
export class LightboxPage implements OnInit {
  @Input() src:string;
  @Input() title:string;
  @Input() add:string;
  @Input() map:string;
  constructor(private mod:ModalController) { }

  ngOnInit() {
    //console.log(this.src);
  }

  dismissModal(){
    this.mod.dismiss();
  }

}
