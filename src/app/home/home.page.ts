import { Component, ViewChild, ElementRef, Renderer2 } from '@angular/core';
import { IonSlides, ModalController, IonNav, IonContent, LoadingController, AlertController } from '@ionic/angular';
import { ActivatedRoute, Router } from '@angular/router';
import { HeaderComponent } from '../components/header/header.component';
import { LightboxPage } from '../lightbox/lightbox.page';
import { BlogPage } from '../blog/blog.page';
import { GalleryPage } from '../gallery/gallery.page';
import { PrivacyPolicyPage } from '../privacy-policy/privacy-policy.page';
import { TermsConditionsPage } from '../terms-conditions/terms-conditions.page';
import { ApiService } from '../api.service';
declare var mobilecheck;
declare var base_href;

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
  providers: [ApiService]
})

export class HomePage {
    isMobile = mobilecheck();
    speechBub:boolean = false;
    @ViewChild(HeaderComponent) hdr: HeaderComponent;
    @ViewChild('gfield') gfield: ElementRef;
    @ViewChild('slider') slider:IonSlides; 
    @ViewChild(IonContent) content;
    msc;
    jumps = ["", "our_story","benefits","our-sites","blog","gallery","contact-us","ready_to_apply"];
    sources = [
    "Social Media",
    "Online Job Portals",
    "Email",
    "School Events",
    "Outdoor Advertising",
    "Print Advertising",
    "Applicant Referral",
    "Employee Referral"];

    jobsPerSite:any = [];
    jobSelected:string = "";

    campaigns = [
      {id:4282,name:"Bridgetowne"},
      {id:4283,name:"Clark"},
      {id:4284,name:"DV - CENTRALE"},
      {id:4285,name:"DV - ROB DELTA"},
      {id:4286,name:"DV - ROB"},
      {id:4287,name:"DV - SM"},
      {id:4288,name:"Makati"},
      {id:4289,name:"MOA"},
      {id:4290,name:"Quezon City"}
    ];
    campaignId:number = 0;
    formdata = {
        first_name:'',
        last_name:'',
        email:'',
        user_phone_number:'',
        //temporary_cv:{},
        source:"vxi-family.com",
        others:{
          specific_source:"",
          position_applying_for:"",
          preferred_work_location:""
        }
      }

    acceptedTerms:boolean = false;

    sectionAboutUs:boolean = true;
    sectionBenefits:boolean = true;
    sectionContactUs:boolean = true;
    sectionGallery:boolean = true;
    sectionJobs:boolean = true;
    sectionNews:boolean = true;
    sectionPassion:boolean = true;
    sectionSites:boolean = true;

    newsSlice:number = (this.isMobile ? 2 : 4);
    curInd:number = 0;
    galBuf:number = 3;
    galMul:number = 370;
  benefits:any = [];

    featInd:number = 0;
    blogPg:number = 0;
    featPages:number = 0;
    blogPages:number = 0;

    blogs:Array<any> = [];

    feat:any = [];

    gal:any = [];

    sites:any = [];
    slideOpts;
    titleName;
    siteIndex:number = 0;

    /* SPIELS */
    ourStory:any;
    passion:any;
    contacts:any;
    jobSpiels:any;

    load;
    appId:any;
    corporate:boolean = false;
    modalOpen:boolean = false;
    resizing;
    applyFor;
    pWork;
  constructor(private render:Renderer2, private modal: ModalController,private route: ActivatedRoute, private router: Router, private api:ApiService, private loader:LoadingController, private alert:AlertController) {
      let self = this;
      this.curInd = 0;

      this.api.getSites().subscribe((a)=>{
          self.sites = a;
          //console.log(self.sites, a);
          self.titleName = self.sites[0].title;
      });

      this.api.getBenefits().subscribe((a)=>{
          this.benefits = a;
      })

      this.api.getNews().subscribe((a)=>{
          let ret:any = a;
          ret.map((b,i)=>{
              //console.log(i,i%this.newsSlice);
              //if(i % (this.newsSlice+1) == 0) self.feat.push(b);
              if(b.featured == 1) self.feat.push(b);
              else self.blogs.push(b);
          });

          self.blogPages = Math.ceil(self.blogs.length / self.newsSlice);
      })

      this.api.getGallery().subscribe((a)=>{
          self.gal = a;
      });

      this.api.getOurStory().subscribe((t)=>{
        self.ourStory = t;
      });

      this.api.getPassion().subscribe((t)=>{
        self.passion = t;
      });

      this.api.getContact().subscribe((t)=>{
        self.contacts = t;
      });

      this.api.getJobSpiels().subscribe((t:any)=>{
        self.sources = t.form_question.split(",");
        self.jobSpiels = t;
      });

      this.api.getMisc().subscribe((t)=>{
        self.msc = t;
      })
  }

  log($event){
    //console.log(this.formdata,$event);
  }

  logName($event){
    let k = $event.key;
    if(/[A-Za-z0-9\-\.]/g.test(k)) return true;
    else return false;
    console.log(this.formdata,$event.key);
  }

  logg($event){
    if(typeof $event == 'undefined' || $event == '') return ;
    let ind = $event.value.split("_");
    let rl = this.jobsPerSite[ind[1]].role;
    this.applyFor = ind[0];
    console.log(rl,this.applyFor);
    if(rl == "corporate") this.corporate = true;
  }

  popJobs(e){
    if(typeof e == 'undefined' || e=='') return ;
    let self = this;
    let i = e.value;
    
    this.formdata.others.position_applying_for = "";
    this.jobsPerSite = [];
    let b = this.sites.filter(a=>{
      //console.log(a);
      if(a.campaign_id == i){
        return a
      }
    })
    this.api.getJobs(b[0].id).subscribe(s=>{
      this.jobsPerSite = s;
    });
  }

  checkPattern($event){
    let k = $event.key;
    if(/[\d]/g.test(k)) return true;
    else return false;
    //$event.target.value = $event.target.value.replace(/[^\d]/g,'');
  }

  uploadFile($event) {
    // Get a reference to the file that has just been added to the input
    //console.log($event);
    let f = $event.target.files[0];
    let self = this;
    // Create a form data object using the FormData API
    new Promise((resolve,reject)=>{      
      let reader = new FileReader();
      reader.readAsDataURL(f);
      reader.onload = () =>{
        resolve(reader.result);
      }
    }).then(file=>{
      //self.formdata.temporary_cv = file;
    })
  }

  async presentToast(message,color) {
    let hd = 'Notice';
    if(color == "success") hd = 'Application Sent';
    let alert = await this.alert.create({
      cssClass: color,
      header: hd,
      message: message,
      buttons: [
        {
          text: 'Okay',
          handler: () => {
            //console.log('Confirm Okay');
          }
        }
      ]
    });

    await alert.present();
  }

  async presentLoading() {
    this.load = await this.loader.create({
      cssClass: 'my-custom-class',
      message: 'Submitting Application...',
      backdropDismiss: false,
      spinner:'bubbles'
    });
    await this.load.present();

    const { role, data } = this.load.onDidDismiss();
    //console.log('Loading dismissed!');
  }

  submitCampaign(){
    //this.formdata.user_phone_number = this.formdata.user_phone_number.toString();
    this.presentLoading();
    let self = this;    
    if(this.corporate){
      this.campaignId = 4330;
      this.formdata.others.preferred_work_location = "Corporate";
    }else{
      let filt = self.sites.filter((a,b)=>{
        return a.campaign_id == self.campaignId;
      })
      this.formdata.others.preferred_work_location = filt[0].title;
    }
    this.formdata.others.position_applying_for = self.applyFor;
    let t = this.formdata;
    let recData = new FormData();
    recData.append('first_name',t.first_name);
    recData.append('last_name',t.last_name);
    recData.append('email',t.email);
    recData.append('user_phone_number',t.user_phone_number);
    recData.append('specific_source',t.others.specific_source);
    recData.append('position',t.others.position_applying_for);
    recData.append('loc',t.others.preferred_work_location);
    recData.append('source',t.source);
    recData.append('campaign_id',this.campaignId.toString());
    const object: any = {};
    recData.forEach(function(value, key){
      object[key] = value;
    });
    object.others = t.others;
    console.log(this.formdata, recData, object);
    fetch(`${base_href}vxiBot/api/vxi/web_enroll_candidate`, {
      method: "POST",
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({data: object})
    }).then((data) => {
      self.clearData()
      this.load.dismiss();
      this.presentToast('Thank you for submitting your application.','success');
    }).catch((err) => {
      this.load.dismiss();
      if(err.hasOwnProperty('error') && err.status != 500){
        this.presentToast(err.error,'danger');
      }else{
        this.presentToast('An error has occurred. Please recheck your details and try re-submitting later.','danger');
      }
    });

      //record to our db
    /* this.api.recordApplication(recData).subscribe((d)=>{
      self.appId = d;
        //submit to talkpush
      t.others.position_applying_for = self.applyFor;
      console.log(t);
      let fd = new FormData();
      self.api.submitApplication(this.campaignId,t).subscribe((tz:any)=>{
        fd.append('id',self.appId);
        fd.append('status',JSON.stringify(tz));
        self.api.updateAppStatus(fd).subscribe(s=>{
          console.log(s);
          self.clearData()
        });
        this.load.dismiss();
        if(tz.hasOwnProperty('error')){
          this.presentToast('Your application is not recorded. A similar application is already submitted.','danger');
        }else{
          this.presentToast('Thank you for submitting your application.','success');
        }
      }, err=>{
        fd.append('id',self.appId);
        fd.append('status',JSON.stringify(err));
        self.api.updateAppStatus(fd).subscribe(s=>{
          console.log(s);
          self.clearData()
        });
        this.load.dismiss();
        if(err.hasOwnProperty('error') && err.status != 500){
          this.presentToast(err.error,'danger');
        }else{
          this.presentToast('An error has occurred. Please recheck your details and try re-submitting later.','danger');
        }
      });
    },err=>{

    }); */
    
  }

  clearData(){
    this.formdata = {
        first_name:'',
        last_name:'',
        email:'',
        user_phone_number:'',
        //temporary_cv:{},
        source:"vxi-family.com",
        others:{
          specific_source:"",
          position_applying_for:"",
          preferred_work_location:""
        }
      };
    this.campaignId = 0;
    this.corporate = false;
    this.acceptedTerms = false;
  }

  toPage(page,hash:number = 0){
      let ind = (hash < 0 ? 0 : this.sites[hash].id);
      this.router.navigate([page], {queryParams:{j:ind}}).then(()=>{
    sessionStorage.p = page;
        
      });
  }

  async presentModal(src:string,title:string,add:string,map:string){
    let self = this;
      if(self.modalOpen) return ;
      const mod = await this.modal.create({
          component:LightboxPage,     
          //cssClass:'reduced', 
          componentProps: {
              src:src,
              title:title,
              add:add,
              map:map
          }    
      });
      mod.onDidDismiss().then(()=>{
        self.modalOpen = false;
      })
      self.modalOpen = true;
      return await mod.present();
  }

  async readMore(id: any) {
    // this.router.navigate(['blog', id])
    window.open(`${base_href}/blog/${id}`, '_blank');
  }

  async oldReadMore(id:any){
    let self = this;
      if(self.modalOpen) return ;
    let mod = await this.modal.create({
      component:BlogPage,
      componentProps: {
        id:id
      }
    });
    mod.onDidDismiss().then(()=>{
      self.modalOpen = false;
    })
    self.modalOpen = true;
    return await mod.present();
  }

  async showGallery(g:any){
    let self = this;
    if(g.out == 0){
      if(self.modalOpen) return ;
      let mod = await this.modal.create({
        component:GalleryPage,
        componentProps: {
          contents:g
        }
      });
      mod.onDidDismiss().then(()=>{
        self.modalOpen = false;
      })
      self.modalOpen = true;
      return await mod.present();
    }else{
      window.open(g.link);
    }
  }

  async showTerms(){
    let self = this;
    if(self.modalOpen) return ;
    let mod = await this.modal.create({
        component:TermsConditionsPage
    });
    mod.onDidDismiss().then(()=>{
      self.modalOpen = false;
    })
    self.modalOpen = true;
    return await mod.present();
  }

  async showPp(){
    let self = this;
    if(self.modalOpen) return ;
    let mod = await this.modal.create({
        component:PrivacyPolicyPage
    });
    mod.onDidDismiss().then(()=>{
      self.modalOpen = false;
    })
    self.modalOpen = true;
    return await mod.present();
  }

  ionViewDidEnter(){
      let self = this;
      let j = parseInt(this.route.snapshot.queryParams.j);
      //console.log(j);      
      if(j>0) document.querySelector(`#${self.jumps[j]}`).scrollIntoView();
  }
  
  ngOnInit(){
      let self = this;
      let j = parseInt(this.route.snapshot.queryParams.j);
      this.acceptedTerms = false;
      
      self.api.getSections().subscribe((t:any)=>{
        let self = this;
        t.map((e:{section:string,status:number,updated:string})=>{
          if(e.section=="jobs") self.sectionJobs = e.status == 1;
          if(e.section=="about_us") self.sectionAboutUs = e.status == 1;
          if(e.section=="benefits") self.sectionBenefits = e.status == 1;
          if(e.section=="contact_us") self.sectionContactUs = e.status == 1;
          if(e.section=="gallery") self.sectionGallery = e.status == 1;
          if(e.section=="blogs") self.sectionNews = e.status == 1;
          if(e.section=="sites") self.sectionSites = e.status == 1;
          if(e.section=="passion") self.sectionPassion = e.status == 1;
        });
        this.hdr.toggleShow('Careers',self.sectionJobs);
        this.hdr.toggleShow('Ourstory',self.sectionAboutUs);
        this.hdr.toggleShow('Benefits',self.sectionBenefits);
        this.hdr.toggleShow('Contactus',self.sectionContactUs);
        this.hdr.toggleShow('Gallery',self.sectionGallery);
        this.hdr.toggleShow('Blogs',self.sectionNews);
        this.hdr.toggleShow('Sites',self.sectionSites);
      })
      setTimeout(function(){
        self.speechBub = true;
        if(location.hash.trim() != "") document.querySelector(location.hash).scrollIntoView();
        //if(j>0) document.querySelector(`#${self.jumps[j]}`).scrollIntoView();
        if(!self.isMobile || window.innerWidth > 768) self.galMul = Math.ceil((self.gfield.nativeElement.offsetWidth-20) / 3) + 10;
      },1000);
      //this.presentToast('Your application is not recorded. A similar application is already submitted.','danger');
      //console.log(this.blogPages)
      if(this.isMobile && window.innerWidth <= 768){
          this.galBuf = 1;
          this.galMul = 260;
      }
      this.slideOpts = {
        loop: true,
        speed:500,
        duration:5000,
        autoplay:{
          disableOnInteraction:false
        },
    pagination: {
      el: ".swiper-pagination",
      type: "bullets",
      clickable: true
    },
        on: {
    beforeInit() {
      const swiper = this;
      swiper.classNames.push(`${swiper.params.containerModifierClass}fade`);
      const overwriteParams = {
        slidesPerView: 1,
        slidesPerColumn: 1,
        slidesPerGroup: 1,
        watchSlidesProgress: true,
        spaceBetween: 0,
        virtualTranslate: true,
      };
      swiper.params = Object.assign(swiper.params, overwriteParams);
      swiper.params = Object.assign(swiper.originalParams, overwriteParams);
    },
    setTranslate() {
      const swiper = this;
      const { slides } = swiper;
      for (let i = 0; i < slides.length; i += 1) {
        const $slideEl = swiper.slides.eq(i);
        const offset$$1 = $slideEl[0].swiperSlideOffset;
        let tx = -offset$$1;
        if (!swiper.params.virtualTranslate) tx -= swiper.translate;
        let ty = 0;
        if (!swiper.isHorizontal()) {
          ty = tx;
          tx = 0;
        }
        const slideOpacity = swiper.params.fadeEffect.crossFade
          ? Math.max(1 - Math.abs($slideEl[0].progress), 0)
          : 1 + Math.min(Math.max($slideEl[0].progress, -1), 0);
        $slideEl
          .css({
            opacity: slideOpacity,
          })
          .transform(`translate3d(${tx}px, ${ty}px, 0px)`);
      }
    },
    setTransition(duration) {
      const swiper = this;
      const { slides, $wrapperEl } = swiper;
      slides.transition(duration);
      if (swiper.params.virtualTranslate && duration !== 0) {
        let eventTriggered = false;
        slides.transitionEnd(() => {
          if (eventTriggered) return;
          if (!swiper || swiper.destroyed) return;
          eventTriggered = true;
          swiper.animating = false;
          const triggerEvents = ['webkitTransitionEnd', 'transitionend'];
          for (let i = 0; i < triggerEvents.length; i += 1) {
            $wrapperEl.trigger(triggerEvents[i]);
          }
        });
      }
    },
  }
};

  }

  onScroll(event){
      //console.log(event);
      this.hdr.searchSharp(true);
      this.hdr.toggleMenu(true);
  }

  onresize(event){
      //console.log(event);
    let self = this;
    this.isMobile = mobilecheck();
    if(this.isMobile && window.innerWidth <= 768){
        this.galBuf = 1;
        this.galMul = 260;
    }else{
        this.galBuf = 3;
        this.galMul = Math.ceil((this.gfield.nativeElement.offsetWidth-20) / 3) + 10;
    }
    self.newsSlice = (this.isMobile ? 2 : 4);
    self.blogPages = Math.ceil(self.blogs.length / self.newsSlice);

    this.resizing = setTimeout(()=>{
      self.slide(-1);
      self.slide(1);
      self.nextBlogs(-1);
      self.nextBlogs(1);
    },500);

    //console.log(this.gfield.nativeElement.offsetWidth);
  }

  slide(i:number){
      if(this.gal.length - this.galBuf <= this.curInd && i>0) return false;
      if(i<0 && this.curInd <= 0) return false; //leftmost already
      this.curInd += i;
      //console.log(this.curInd,i);
      let mul = -this.galMul;
      //console.log(mul, this.galMul,`translateX(${mul*this.curInd}px)`,this.isMobile);
      this.render.setStyle(this.gfield.nativeElement,'transform',`translateX(${mul*this.curInd}px)`);
  }

  nextBlogs(i:number){
      //console.log(this.featInd, this.feat.length,this.blogs.length,  this.blogPg*this.newsSlice);
      if(i>0 && (this.featInd+1 >= this.feat.length) && (this.blogPg >= this.blogPages-1)) return false;
      if(i<0 && this.featInd <=0 && this.blogPg <=0) return false;
      this.featInd += i;
      this.blogPg += i;      
      if(i>0 && this.featInd >= this.feat.length-1) this.featInd = this.feat.length-1;
      if(i>0 && this.blogPg >= this.blogPages-1) this.blogPg = this.blogPages-1;
      if(i<0 && this.featInd <= 0) this.featInd = 0;
      if(i<0 && this.blogPg <= 0) this.blogPg = 0;
  }

  updateTitle(index:number){
      this.titleName = this.sites[index].title;
  }

  updateSiteTitle(event){
      let self = this;
      this.slider.getActiveIndex().then(i=>{
          let n = i-1;
          if(n<0) n = this.sites.length-1;
          if(n>=this.sites.length) n = 0;
          //self.updateTitle(n);
          self.siteIndex = n;
      })
  }

  enhance(fname:string){
      //console.log(fname);
  }

  validphonenumber(){
    let phoneno = /\d{11}$/;
    return (phoneno.test(this.formdata.user_phone_number));
  }

  validemail(){
    let e = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
    return (e.test(this.formdata.email));
  }
}
