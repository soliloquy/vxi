import { Component, OnInit, Renderer2, ElementRef, ViewChild } from '@angular/core';
import {Router, ActivatedRoute} from '@angular/router';
import { ModalController } from '@ionic/angular';
import { SearchPagePage } from '../../search-page/search-page.page';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
})
export class HeaderComponent implements OnInit {

  @ViewChild('menu') menu: ElementRef;
  showMenu:boolean = false;
  showSearch:boolean = false;
  shows = {Ourstory: false,
  Benefits:false,
  Sites:false,
  Careers:false,
  Blogs:false,
  Gallery:false,
  Contactus:false};
  s:string = '';

    jumps = ["", "our_story","benefits","our-sites","blog","gallery","contact-us","ready_to_apply"];
  constructor(private render: Renderer2, private router:Router, private route:ActivatedRoute, private mod:ModalController) { }

  ngOnInit() {}

  test($event){
    this.s = $event.detail.value;
    //console.log($event,this.s);
  }

  async doSearch($event){
    //console.log($event);
    if($event != '13') return false;
    let mod = await this.mod.create({
        component:SearchPagePage,
        componentProps:{
          search:this.s
        }
    });
    return await mod.present();
  }

  toPage(page,hash:number = 0){
    //if(hash == 0) page = 'home';
    if(hash == 0){
      this.router.navigate([page], {queryParams:{j:0}}).then(()=>{
    sessionStorage.p = page;
        
      });
    }else{
      if(sessionStorage.p == "/"){
        document.querySelector('#'+this.jumps[hash]).scrollIntoView();
      }
      
      else{
        this.router.navigate(['/'], {queryParams:{j:hash}}).then(()=>{
    sessionStorage.p = page;
        
      });;
      }
    }
  }

  toggleShow(e,status:boolean){
    this.shows[e] = status;
  }

  toggleMenu(forceOff:boolean = false){

    if(forceOff){
      this.showMenu = false;
      return ;
    }

    this.showMenu = !this.showMenu;
    if(this.showMenu) this.showSearch = false;
  }

  searchSharp(forceOff:boolean = false){
    //console.log(forceOff);
    if(forceOff){
      this.showSearch = false;
      return ;
    }

    this.showSearch = !this.showSearch;
    if(this.showSearch) this.showMenu = false;
  }

}
