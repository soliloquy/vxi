import { CommonModule } from '@angular/common';
import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { IonicModule } from '@ionic/angular';
import { HeaderComponent } from './header/header.component';
import { SearchPagePageModule } from '../search-page/search-page.module';
@NgModule({
    declarations: [HeaderComponent],
    schemas: [ CUSTOM_ELEMENTS_SCHEMA ],
    imports: [
        CommonModule,
        //FormsModule,
        SearchPagePageModule
    ],
    exports: [HeaderComponent]
})
export class ShareComponentModule {}