import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { SearchPagePageRoutingModule } from './search-page-routing.module';
//import { ShareComponentModule } from '../components/share-component.module';

import { SearchPagePage } from './search-page.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    SearchPagePageRoutingModule,
    //ShareComponentModule
  ],
  declarations: [SearchPagePage]
})
export class SearchPagePageModule {}
