import { Component, OnInit, ViewChild, ElementRef, Input, Renderer2 } from '@angular/core';
import {Router, ActivatedRoute} from '@angular/router';
import { ModalController } from '@ionic/angular';
import { HeaderComponent } from '../components/header/header.component';
import { ApiService } from '../api.service';
declare var mobilecheck;
@Component({
  selector: 'app-search-page',
  templateUrl: './search-page.page.html',
  styleUrls: ['./search-page.page.scss'],
})
export class SearchPagePage implements OnInit {

    @Input() search:string;
    @ViewChild('body') body: ElementRef;
    isMobile = mobilecheck();
    benefits:any = [];
    jobs:any = [];
    blogs:any = [];
    gallery:any = [];
    sites:any = [];
  constructor(private api:ApiService, private router:Router, private route:ActivatedRoute,private mod:ModalController, private render:Renderer2) { }

  ngOnInit() {
    let self = this;
    this.api.searchBenefits(this.search).subscribe((t:any)=>{
      self.benefits = t.map((a)=>{
        return {
          img:a.img,
          id:a.id,
          subtext:a.subtext.replace(new RegExp(self.search,"ig"),function(match){
            return self.matchCase(match)
          }),
          text:a.text.replace(new RegExp(self.search,"ig"),function(match){
            return self.matchCase(match)
          })
        }
      });
      // console.log(self.benefits);
    });

    this.api.searchJobs(this.search).subscribe((t:any)=>{
      self.jobs = t.map((a)=>{
        return {
          // desc:a.desc.replace(new RegExp(self.search,"ig"),`<i>${self.search}</i>`),
          desc:a.desc.replace(new RegExp(self.search,"ig"),function(match){
            return self.matchCase(match)
          }),
          id:a.id,
          location:a.location.replace(new RegExp(self.search,"ig"),function(match){
            return self.matchCase(match)
          }),
          qual:a.qual.replace(new RegExp(self.search,"ig"),function(match){
            return self.matchCase(match)
          }),
          role:a.role.replace(new RegExp(self.search,"ig"),function(match){
            return self.matchCase(match)
          }),
          slots:a.slots.replace(new RegExp(self.search,"ig"),function(match){
            return self.matchCase(match)
          }),
          title:a.title.replace(new RegExp(self.search,"ig"),function(match){
            return self.matchCase(match)
          })
        }
      });
      // console.log(t);
    });

    this.api.searchBlogs(this.search).subscribe((t:any)=>{
      self.blogs = t.map((a)=>{
        return {
          head:a.head.replace(new RegExp(self.search,"ig"),function(match){
            return self.matchCase(match)
          }),
          id:a.id,
          img:a.img,
          date:a.date.replace(new RegExp(self.search,"ig"),function(match){
            return self.matchCase(match)
          }),
          sub:a.sub.replace(new RegExp(self.search,"ig"),function(match){
            return self.matchCase(match)
          }),
          text:(a.text == null ? null : a.text.replace(new RegExp(self.search,"ig"),function(match){
            return self.matchCase(match)
          }))
        }
      });
      // console.log(self.blogs);
    });

    this.api.searchSites(this.search).subscribe((t:any)=>{
      self.sites = t.map((a)=>{
        return {
          head:a.title.replace(new RegExp(self.search,"ig"),function(match){
            return self.matchCase(match)
          }),
          id:a.id,
          img:a.img,
          address:a.address.replace(new RegExp(self.search,"ig"),function(match){
            return self.matchCase(match)
          }),
          desc:(a.desc == null ? null : a.desc.replace(new RegExp(self.search,"ig"),function(match){
            return self.matchCase(match)
          })),
          map:a.map
        }
      });
      // console.log(self.blogs);
    });

    this.api.searchGallery(this.search).subscribe((t:any)=>{
      self.gallery = t.map((a)=>{
        return {
          //details:a.details.replace(new RegExp(self.search,"ig"),`<i>${self.search}</i>`),
          details:a.details.replace(new RegExp(self.search,"ig"),function(match){
            return self.matchCase(match)
          }),
          id:a.id,
          img:a.img,
          link:a.link,
          //title:a.title.replace(new RegExp(self.search,"ig"),`<i>${self.search}</i>`)
          title:a.title.replace(new RegExp(self.search,"ig"),function(match){
            return self.matchCase(match)
          })
        }
      });
      // console.log(t);
    });

    //this.render.setProperty(this.body.nativeElement,"innerHTML","<br/>");
  }

  matchCase(text):string {
    let result = `<i>${text}</i>`;
    return result;
  }

  dismissModal(){
    this.mod.dismiss();
  }

  onresize(event){
      //console.log(event);
      this.isMobile = mobilecheck();
  }


  toPage(page,hash:number = 0){
    //if(hash == 0) page = 'home';
    let self = this;
    if(hash == 0){
      this.router.navigate([page], {queryParams:{j:hash}}).then(()=>{
    sessionStorage.p = page;
        
      }).then(()=>{
        self.dismissModal();
      });
    }else{
      if(sessionStorage.p == 'careers'){
        sessionStorage.p = '';
        window.location.href = './#'+page;
      }else{        
        document.getElementById(page).scrollIntoView();
      }
      self.dismissModal();
    }
  }
}
