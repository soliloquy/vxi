import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { HeaderComponent } from '../components/header/header.component';
import { ApiService } from '../api.service';
declare var mobilecheck;
@Component({
  selector: 'app-terms-conditions',
  templateUrl: './terms-conditions.page.html',
  styleUrls: ['./terms-conditions.page.scss'],
})
export class TermsConditionsPage implements OnInit {
  isMobile:boolean;
  text:any = {};
  constructor(private api:ApiService, private mod:ModalController) { }

  ngOnInit() {
    this.api.getTerms().subscribe((t)=>{
      this.text = t;
    })
  }

  dismissModal(){
    this.mod.dismiss();
  }

  onresize(event){
      //console.log(event);
      this.isMobile = mobilecheck();
  }
}
