import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { TermsConditionsPageRoutingModule } from './terms-conditions-routing.module';
import { ShareComponentModule } from '../components/share-component.module';

import { TermsConditionsPage } from './terms-conditions.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    TermsConditionsPageRoutingModule,
    ShareComponentModule
  ],
  declarations: [TermsConditionsPage]
})
export class TermsConditionsPageModule {}
