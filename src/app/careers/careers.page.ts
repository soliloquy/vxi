import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
//import {  } from '@ionic/angular';
import { Router, ActivatedRoute} from '@angular/router';
import { HeaderComponent } from '../components/header/header.component';
import { ApiService } from '../api.service';
import Accordion from 'accordion-js';
import 'accordion-js/dist/accordion.min.css';
declare var mobilecheck;
@Component({
  selector: 'app-careers',
  templateUrl: './careers.page.html',
  styleUrls: ['./careers.page.scss'],
})
export class CareersPage implements OnInit {

    isMobile = mobilecheck();
  @ViewChild('jobs') jobs:ElementRef;
  @ViewChild(HeaderComponent) hdr: HeaderComponent;
  locations:any = [
    {ind:'cl',text:'Clark'},
    {ind:'ce',text:'Centrale'},
    {ind:'moa',text:'Mall of Asia'},
    {ind:'pa',text:'Panorama'},
    {ind:'br',text:'Bridgetown'},
    {ind:'de',text:'Delta'},
    {ind:'ma',text:'Makati'},
    {ind:'ec',text:'SM Ecoland'}
  ];
  loc:any = 0;
  locB:any = [];
  jobList:any = [];
  corporateList:any = [];
  joInd:any;
  campaigns:any = [];
  jobSpiels:any = [];
  accord;

  sectionAboutUs:boolean = true;
  sectionBenefits:boolean = true;
  sectionContactUs:boolean = true;
  sectionGallery:boolean = true;
  sectionJobs:boolean = true;
  sectionNews:boolean = true;
  sectionPassion:boolean = true;
  sectionSites:boolean = true;

  constructor(private api:ApiService, private act:ActivatedRoute) {
    let self = this;
    this.api.getSites().subscribe((a:any)=>{
      self.locations = a;
      a.map((l,i)=>{
        self.locB[l.id] = l;
      });
    });
    //if(!this.isMobile && this.act.snapshot.queryParams.j) this.showJobs(1);
  }

  async ngOnInit() {
   /* this.api.getCampaigns().subscribe((t:any)=>{
      this.campaigns = t.campaigns;
    })*/
    let self = this;
    this.showJobs( this.act.snapshot.queryParams.j );

    await this.api.getJobSpiels().subscribe((t)=>{
      this.jobSpiels = t;
      //new Accordion('.expander');
    });
      this.accord = new Accordion('.container');
      
    self.api.getSections().subscribe((t:any)=>{
      let self = this;
      t.map((e:{section:string,status:number,updated:string})=>{
        if(e.section=="jobs") self.sectionJobs = e.status == 1;
        if(e.section=="about_us") self.sectionAboutUs = e.status == 1;
        if(e.section=="benefits") self.sectionBenefits = e.status == 1;
        if(e.section=="contact_us") self.sectionContactUs = e.status == 1;
        if(e.section=="gallery") self.sectionGallery = e.status == 1;
        if(e.section=="blogs") self.sectionNews = e.status == 1;
        if(e.section=="sites") self.sectionSites = e.status == 1;
        if(e.section=="passion") self.sectionPassion = e.status == 1;
      });
      this.hdr.toggleShow('Careers',self.sectionJobs);
      this.hdr.toggleShow('Ourstory',self.sectionAboutUs);
      this.hdr.toggleShow('Benefits',self.sectionBenefits);
      this.hdr.toggleShow('Contactus',self.sectionContactUs);
      this.hdr.toggleShow('Gallery',self.sectionGallery);
      this.hdr.toggleShow('Blogs',self.sectionNews);
      this.hdr.toggleShow('Sites',self.sectionSites);
    })
  }

  onresize(event){
      //console.log(event);
      this.isMobile = mobilecheck();
  }

  async showJobsE(e){
    let self = this;
    let i = e.value;
    this.loc = i;
    this.joInd = 0;
    this.corporateList = [];
    this.jobList = [];
    await this.api.getJobs(i).subscribe((a)=>{
      let jobs:any = a;
      jobs.map((j)=>{
        // let k = j;
        // k.qual = JSON.parse(k.qual);
        //console.log(k);
        if(j.role == "agent") self.jobList.push( j );
        else self.corporateList.push(j);
      });
      this.accord = new Accordion('.container');
    })

  }

  async showJobs(i){
    let self = this;
    this.loc = i;
    this.joInd = 0;
    this.corporateList = [];
    this.jobList = [];
    await this.api.getJobs(i).subscribe((a)=>{
      let jobs:any = a;
      jobs.map((j)=>{
        // let k = j;
        // k.qual = JSON.parse(k.qual);
        //console.log(k);
        if(j.role == "agent") self.jobList.push( j );
        else self.corporateList.push(j);
      });
    })
      this.accord = new Accordion('.container');
  }

  apply(i){
    //console.log(i);
    location.href = "?j=7";
  }

  setJo(i){
    if(i == this.joInd){
      this.joInd = "";
      return ;
    }
    this.joInd = i;
  }
}
