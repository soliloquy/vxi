import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';

import { HeaderComponent } from '../components/header/header.component';
import { ApiService } from '../api.service';
declare var mobilecheck;
@Component({
  selector: 'app-privacy-policy',
  templateUrl: './privacy-policy.page.html',
  styleUrls: ['./privacy-policy.page.scss'],
})
export class PrivacyPolicyPage implements OnInit {

  isMobile:boolean;
  text:any = [];
  constructor(private api:ApiService, private mod:ModalController) { }

  ngOnInit() {
    this.api.getPp().subscribe((t)=>{
      this.text = t;
    })
  }

  dismissModal(){
    this.mod.dismiss();
  }

  onresize(event){
      //console.log(event);
      this.isMobile = mobilecheck();
  }
}
