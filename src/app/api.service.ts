import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpRequest, HttpParams } from '@angular/common/http';
declare var base_href;
@Injectable({
  providedIn: 'root'
})
export class ApiService {

  // host:string = host;
  //host:string = 'https://dev.megabots.app/vxi/cms/vxi';
  host:string = `${base_href}cms/vxi`;
  talkpush:string = 'https://my.talkpush.com/api/talkpush_services';
  apiKey:string = 'e1cd95479f43e0a3c06739fdd82a3248';
  constructor(private http:HttpClient) {

  }

  getBenefits(){
    let hdr = new HttpHeaders();
    hdr.append('Content-Type','application/json');
    return this.http.get(`${this.host}/get_benefits`,{headers:hdr});
  }

  getMisc(){
    let hdr = new HttpHeaders();
    hdr.append('Content-Type','application/json');
    return this.http.get(`${this.host}/get_misc`,{headers:hdr});
  }

  getNews(){
    let hdr = new HttpHeaders();
    hdr.append('Content-Type','application/json');
    return this.http.get(`${this.host}/get_news`,{headers:hdr});
  }

  getPp(){
    let hdr = new HttpHeaders();
    hdr.append('Content-Type','application/json');
    return this.http.get(`${this.host}/get_pp`,{headers:hdr});
  }

  getTerms(){
    let hdr = new HttpHeaders();
    hdr.append('Content-Type','application/json');
    return this.http.get(`${this.host}/get_terms`,{headers:hdr});
  }

  getGallery(){
    let hdr = new HttpHeaders();
    hdr.append('Content-Type','application/json');
    return this.http.get(`${this.host}/get_gallery`,{headers:hdr});
  }

  getSites(){
    let hdr = new HttpHeaders();
    hdr.append('Content-Type','application/json');
    return this.http.get(`${this.host}/get_sites`,{headers:hdr});
  }

  getJobs(s:number = 0){
    let hdr = new HttpHeaders();
    hdr.append('Content-Type','application/json');
    return this.http.get(`${this.host}/get_jobs?g=${s}`,{headers:hdr});
  }

  getBlog(s:number|string){
    let hdr = new HttpHeaders();
    hdr.append('Content-Type','application/json');
    return this.http.get(`${this.host}/get_blog?id=${s}`,{headers:hdr});
  }

  getJobSpiels(){
    let hdr = new HttpHeaders();
    hdr.append('Content-Type','application/json');
    return this.http.get(`${this.host}/get_job_spiels`,{headers:hdr});
  }

  getOurStory(){
    let hdr = new HttpHeaders();
    hdr.append('Content-Type','application/json');
    return this.http.get(`${this.host}/get_our_story`,{headers:hdr});
  }

  getPassion(){
    let hdr = new HttpHeaders();
    hdr.append('Content-Type','application/json');
    return this.http.get(`${this.host}/get_passion`,{headers:hdr});
  }

  getContact(){
    let hdr = new HttpHeaders();
    hdr.append('Content-Type','application/json');
    return this.http.get(`${this.host}/get_contact`,{headers:hdr});
  }

  getSections(){
    let hdr = new HttpHeaders();
    hdr.append('Content-Type','application/json');
    return this.http.get(`${this.host}/get_sections`,{headers:hdr});
  }

  getCampaigns(){
    let hdr = new HttpHeaders();
    hdr.append('Content-Type','application/json');
    return this.http.get(`${this.talkpush}/campaigns?api_key=${this.apiKey}`,{headers:hdr});

  }

  // unused
  submitApplication(cid:number,data){
    let hdr = new HttpHeaders();
    hdr.append('Content-Type','application/json');

    let bdy = {
      api_key:this.apiKey,
      campaign_invitation:data
    };
    return this.http.post(`${this.talkpush}/campaigns/${cid}/campaign_invitations?api_key=${this.apiKey}`,bdy,{headers:hdr});

  }
  
  // unused
  recordApplication(data){
    data.append('api_key',this.apiKey);
    let hdr = new HttpHeaders();
    hdr.append('Content-Type','application/x-www-form-urlencoded');

    return this.http.post(`${this.host}/new_application`,data,{headers:hdr});
  }

  updateAppStatus(data){
    let hdr = new HttpHeaders();
    hdr.append('Content-Type','application/x-www-form-urlencoded');
    
    return this.http.post(`${this.host}/update_application_status`,data,{headers:hdr});
  }

  searchJobs(s:string = ''){
    let hdr = new HttpHeaders();
    hdr.append('Content-Type','application/json');
    return this.http.get(`${this.host}/search_jobs?search=${s}`,{headers:hdr});
  }

  searchBlogs(s:string = ''){
    let hdr = new HttpHeaders();
    hdr.append('Content-Type','application/json');
    return this.http.get(`${this.host}/search_blogs?search=${s}`,{headers:hdr});
  }

  searchSites(s:string = ''){
    let hdr = new HttpHeaders();
    hdr.append('Content-Type','application/json');
    return this.http.get(`${this.host}/search_sites?search=${s}`,{headers:hdr});
  }

  searchGallery(s:string = ''){
    let hdr = new HttpHeaders();
    hdr.append('Content-Type','application/json');
    return this.http.get(`${this.host}/search_gallery?search=${s}`,{headers:hdr});
  }

  searchBenefits(s:string = ''){
    let hdr = new HttpHeaders();
    hdr.append('Content-Type','application/json');
    return this.http.get(`${this.host}/search_benefits?search=${s}`,{headers:hdr});
  }

}
