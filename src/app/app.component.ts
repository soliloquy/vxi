import { Component, ViewChild, ElementRef, Renderer2 } from '@angular/core';
import { Platform, MenuController, AlertController, NavController } from '@ionic/angular';
import { FacebookService, InitParams } from 'ngx-facebook';

declare var pageId;
declare var mobilecheck; //fn to check for screen type
@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss'],
})
export class AppComponent {
  isMobile : boolean = mobilecheck();
  node = document.querySelector('body');
  pageid = pageId;
  @ViewChild('fb') fb: ElementRef;
  constructor(platform: Platform, alert:AlertController, private facebookService: FacebookService, private render:Renderer2) {
    let self = this;
    let int;
    let al;
    let activeAlert:boolean = false;
    platform.ready().then(() => {
        // Okay, so the platform is ready and our plugins are available.
        // Here you can do any higher level native things you might need.
        /*statusBar.styleDefault();
        splashScreen.hide();*/
        self.mcheck();
        window.onresize = function(){
          self.mcheck();
        }
        //console.log(self.fb);
        //self.render.setAttribute(self.fb.nativeElement, 'page_id', pageId);

        //used to check periodically for a working net connection
        /*int = window.setInterval(()=>{
        if(this.netCheck()){
          try{
            al.dismiss();
            activeAlert = false;
          }catch(e){
            activeAlert = false;
          }
        }else{
          if(activeAlert) return false;
          self.presentAlert(alert);
          activeAlert = true;
        }
      },1000);*/


      });
    }

  ngOnInit(){
    //this.initFacebookService();
  }

  private initFacebookService(): void {
    const initParams: InitParams = { xfbml:true, version:'v11.0', appId:this.pageid};
    this.facebookService.init(initParams);
  }

  async presentAlert(alert:AlertController) {
    const al = await alert.create({
      header: 'No internet connection',
      message: 'A stable internet connection is needed to use this site.',
      buttons: ['OK']
    });

    await al.present();

    const { role } = await al.onDidDismiss();
    console.log('onDidDismiss resolved with role', role);
  }


  reorient($event){
    this.isMobile = mobilecheck();
  }

  mcheck(){
    let e:Window = window;
   if(e.innerWidth <= 1024 && e.innerWidth > 768){
     sessionStorage.view = 'desktop';
     this.node.className = this.node.className.replace(/[\s]mobile-view/g,"");
     this.node.className = this.node.className.replace(/[\s]lgmd/g,"");
     this.node.className = this.node.className.replace(/[\s]mini/g,"");
     this.node.className += " lgmd";

   }else if(e.innerWidth <= 768){
     sessionStorage.view = 'mobile';
     this.node.className = this.node.className.replace(/[\s]mobile-view/g,"");
     this.node.className = this.node.className.replace(/[\s]lgmd/g,"");
     this.node.className = this.node.className.replace(/[\s]mini/g,"");
     this.node.className += " mobile-view";
     if(e.innerWidth <= 540){
       this.node.className += " mini";
     }
   }else{
     sessionStorage.view = 'desktop';
     this.node.className = this.node.className.replace(/[\s]mobile-view/g,"");
     this.node.className = this.node.className.replace(/[\s]lgmd/g,"");
     this.node.className = this.node.className.replace(/[\s]mini/g,"");
   }
  }

  netCheck():boolean{
    if(window.navigator.onLine){
      return true;
    }else{
      return false;
    }
  }
}
